# Simple React Boilerplate
Clone this repo:
```sh
git clone git@gitlab.com:Rub3nc/simple-react-boilerplate.git
cd simple-react-boilerplate
```

Install the dependencies:
```sh
npm install
```

Run webpack:
```sh
webpack -w
```

Head over your browser at *http://localhost:3000* and test that everything is working correctly.
